Contributors
============

* Élise Brodin <> [ebrodin]
* Florent Michon <flohcim@gmail.com> [flohcim][fmichon]
* Jean-Yves Madier de Champvermeil <> [jymadier]
* Jérémy Cérini <> [cerini_j][jcerini]
* Kevin Burles <> [KBurles]
* Matthias Broquet <mbroquet@atreal.fr> [mbroquet][tiazma]
* Michaël Bideau <> [mbideau]
* Nicolas Haye <> [nhaye][NHaye]
* Nicolas Meucci <> [nmeucci][oc1n]
* Rabah Asseum <> [Rasseum]
* Sébastien Dethyre <> [SebastienDethyreReal]
* Sofien Timezouaght <> [softime][stimezouaght]
* Virginie Pihour <> [vpihour]

